
var app = angular.module('BlankApp', ['ngMaterial', 'pascalprecht.translate'])
    

app.config(function ($translateProvider) {
    $translateProvider.translations('en', {
        '_weight': 'Weight',
        '_height': 'Height(cm)',
        '_width': 'Width(cm)',
        '_length': 'Length(cm)',
        '_amount': 'Amount(กล่อง)',
        'cal_action': 'Calculator',
        '_cbm': 'CBM'
    });
    $translateProvider.translations('th', {
        '_weight': 'น้ำหนัก(kg)',
        '_height': 'สูง(cm)',
        '_width': 'กว้าง(cm)',
        '_length': 'ยาว(cm)',
        '_amount': 'จำนวน(กล่อง)',
        'cal_action': 'คำนวน',
        '_cbm': 'CBM'


    });
    $translateProvider.preferredLanguage('th');
})