
app.controller('DemoCtrl', DemoCtrl)
function DemoCtrl($timeout, $q, $log, $http, $scope) {
    var vm = this;
    vm.country_name;
    vm.simulateQuery = false;
    vm.isDisabled = false;
    vm.service_result = [];
    // list of `state` value/display objects
    vm.states = loadAll();
    vm.querySearch = querySearch;
    vm.selectedItemChange = selectedItemChange;

    vm.searchTextChange = searchTextChange;

    vm.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch(query) {
        var results = query ? vm.states.filter(createFilterFor(query)) : vm.states,
            deferred;
        if (vm.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        $log.info('Text changed to ' + text);
        vm.country_name = null
    }

    function selectedItemChange(item) {
        $log.info('Item changed to ' + JSON.stringify(item));
        if (item)
            vm.country_name = item.COUNTRY_NAME
        vm.calculator(vm.criteria_list)
    }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        return country_list
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.uppercase(query);

        return function filterFn(state) {
            return (state.COUNTRY_NAME.indexOf(lowercaseQuery) === 0);
        };

    }




    vm.data = {}
    vm.criteria_list = [{}]
    $scope.$watch(() => vm.criteria_list, (value) => {
        vm.calculator(value)
    }, true)

    vm.calculator = function (criteria_list) {

        vm.service_result = []
        criteria_list.forEach(cal_ex => {
            //var cal_ex = criteria_list[0]
            var _amount = cal_ex._amount || 1
            var _height = Math.round(cal_ex._height)
            var _length = Math.round(cal_ex._length)
            var _weight = cal_ex._weight
            var _width = Math.round(cal_ex._width)
            var _cbm = Math.round(cal_ex._cbm)
            zone_filter = zone.filter(item => item.COUNTRY === vm.country_name)
            service_type.forEach(item => {
                var _size = 0
                if (_width && _length && _height && _amount)
                    _size = vm.calSize(item.SERVICE_ID, _width, _length, _height, _amount)
                var real_weight = _size > _weight ? _size : _weight
                real_weight = real_weight > 20 ? round(real_weight, 1.0) : round(real_weight, 0.5)

                zone_find = zone_filter.find(_item => _item.SERVICE_ID === item.SERVICE_ID)
                console.log('zone_find :: ', zone_find)


                if (zone_find && (zone_find.SERVICE_ID < 7 || zone_find.SERVICE_ID == 12)) {
                    var price_filter
                    if (real_weight > 20) {
                        var _key = vm.more20than(real_weight)
                        price_filter = vm.get_price_data(item.SERVICE_ID, _key)
                    } else {
                        price_filter = vm.get_price_data(item.SERVICE_ID, real_weight)
                    }

                    if (price_filter.length > 0) {
                        if (real_weight > 20) {
                            price_reduce = price_filter[0][_key][zone_find.ZONE] * real_weight
                        } else {
                            price_reduce = price_filter[0][real_weight][zone_find.ZONE]
                        }
                        price_reduce = price_reduce + ""
                        price_reduce = price_reduce.replace(',', '')
                        price_reduce = parseInt(price_reduce)
                        var updatePrice = false
                        vm.service_result.forEach(__item => {
                            if (__item.SERVICE_ID == item.SERVICE_ID){
                                __item.PRICE += price_reduce * _amount; updatePrice = true
                            }
                        })
                        if (!updatePrice) {
                            vm.service_result.push({
                                SERVICE_ID: item.SERVICE_ID,
                                SERVICE_NAME_ENG: item.SERVICE_NAME_ENG,
                                SERVICE_NAME_TH: item.SERVICE_NAME_TH,
                                PRICE: price_reduce * _amount
                            })
                        }
                    }
                }
                if (item.SERVICE_ID == 7) {
                    var price_7_filter = price_7.filter(_item => _item.COUNTRY === vm.country_name)
                    if (price_7_filter && price_7_filter.length > 0) {
                        var _size = 0
                        if (_width && _length && _height && _amount)
                            _size = vm.calSize(7, _width, _length, _height)
                        var real_weight = _size > _weight ? _size : _weight
                        real_weight = round(real_weight, 1.0)
                        price_7_filter.forEach(_item => {
                            var price = 0
                            if (_item['500'] && real_weight < 1000 && real_weight > 500) {
                                price = _item.BASE + (_item['500'] * real_weight)
                            } else if (_item['300'] && real_weight < 500 && real_weight > 300) {
                                price = _item.BASE + (_item['300'] * real_weight)
                            } else if (_item['250'] && real_weight < 300 && real_weight > 250) {
                                price = _item.BASE + (_item['250'] * real_weight)
                            } else if (_item['100'] && real_weight < 250 && real_weight > 100) {
                                price = _item.BASE + (_item['100'] * real_weight)
                            } else if (_item['45'] && real_weight < 100 && real_weight > 45) {
                                price = _item.BASE + (_item['45'] * real_weight)
                            } else if (_item.N && real_weight > _item.N2 && real_weight < 45) {
                                price = _item.BASE + (_item.N * real_weight)
                            } else if (real_weight <= _item.N2) {
                                price = _item.BASE + _item.MIN
                            }
                            var updatePrice = false
                            vm.service_result.forEach(__item => {
                                if (__item.SERVICE_ID == _item.SERVICE_ID && __item.PORT_NAME == _item.PORT_NAME){
                                    __item.PRICE += price * _amount; updatePrice = true
                                }
                            })
                            if (!updatePrice) {
                                vm.service_result.push({
                                    SERVICE_ID :item.SERVICE_ID,
                                    SERVICE_NAME_ENG: item.SERVICE_NAME_ENG,
                                    SERVICE_NAME_TH: item.SERVICE_NAME_TH,
                                    PRICE: price * _amount,
                                    PORT_NAME: _item['PORT_NAME']
                                })
                            }
                        })
                    }
                }
                if (item.SERVICE_ID > 7 && item.SERVICE_ID < 12) {
                    var price_891011_filter = price_891011.filter(_item => _item.COUNTRY === vm.country_name)
                    if (price_891011_filter && price_891011_filter.length > 0) {
                        var CBM = 0
                        if (_width && _length && _height && _amount) {
                            CBM = vm.calCBM(_width, _length, _height)
                        } else {
                            CBM = _weight / 1000
                        }
                        if(_cbm)
                            CBM = _cbm
                        console.log('---CBM--::', CBM)
                        price_891011_filter.forEach(_item => {
                            var price = 0
                            if (CBM < 2) {
                                price = _item.FIRST_CBM8
                            } else {
                                if (item.SERVICE_ID == 8)
                                    price = _item.FIRST_CBM8 + (_item.NEXT_CBM8_1 * (CBM - 1))
                                if (item.SERVICE_ID == 9)
                                    price = _item.FIRST_CBM8 + (_item.Rate_20_9 * (CBM - 1))
                                if (item.SERVICE_ID == 10)
                                    price = _item.FIRST_CBM8 + (_item.Rate_40_10 * (CBM - 1))
                                if (item.SERVICE_ID == 11)
                                    price = _item.FIRST_CBM8 + (_item.Rate_40HQ_11 * (CBM - 1))
                            }
                            var updatePrice = false
                            vm.service_result.forEach(__item => {
                                if (__item.SERVICE_ID == item.SERVICE_ID && __item.PORT_NAME == _item.PORT_NAME){
                                    __item.PRICE += price * _amount; updatePrice = true
                                }
                            })
                            if (!updatePrice) {
                                vm.service_result.push({
                                    SERVICE_ID :item.SERVICE_ID,
                                    SERVICE_NAME_ENG: item.SERVICE_NAME_ENG,
                                    SERVICE_NAME_TH: item.SERVICE_NAME_TH,
                                    PRICE: price * _amount,
                                    PORT_NAME: _item['PORT_NAME']
                                })
                            }
                        })
                    }
                }
            })


        })



    }
    vm.get_price_data = function (service_id, real_weight) {
        var price_filter = []
        switch (service_id) {
            case 1:
                price_filter = price_1.filter(_item => _item[real_weight])
                break
            case 2:
                price_filter = price_2.filter(_item => _item[real_weight])
                break
            case 3:
                price_filter = price_3.filter(_item => _item[real_weight])
                break
            case 4:
                price_filter = price_4.filter(_item => _item[real_weight])
                break
            case 5:
                price_filter = price_5.filter(_item => _item[real_weight])
                break
            case 6:
                price_filter = price_6.filter(_item => _item[real_weight])
                break
            case 12:
                price_filter = price_12.filter(_item => _item[real_weight])
                break
            default:

        }
        return price_filter
    }

    vm.more20than = function (real_weight) {
        if (real_weight > 1000) {
            return '1000+'
        } else if (real_weight > 500) {
            return '500+'
        } else if (real_weight > 400) {
            return '400+'
        } else if (real_weight > 300) {
            return '300+'
        } else if (real_weight > 200) {
            return '200+'
        } else if (real_weight > 100) {
            return '100+'
        } else if (real_weight > 70) {
            return '70+'
        } else if (real_weight > 50) {
            return '50+'
        } else if (real_weight > 30) {
            return '30+'
        } else {
            return '20.5+'
        }
    }
    vm.calSize = function (service_id, w = 0, l = 0, h = 0) {
        var result;
        switch (service_id) {
            case 1:
                result = ((w * l * h) / 5000)
                break
            case 2:
                result = ((w * l * h) / 5000)
                break
            case 3:
                result = ((w * l * h) / 5000)
                break
            case 4:
                result = ((w * l * h) / 5000)
                break
            case 5:
                result = ((w * l * h) / 5000)
                break
            case 6:
                result = ((w * l * h) / 5000)
                break
            case 7:
                result = ((w * l * h) / 6000)
                break
            case 8:
                result = ((w * l * h) / 1000)
                break
            case 9:
                break
            case 10:
                break
            case 11:
                break
            case 12:
                result = ((w * l * h) / 5000)
                break
            default:
        }
        return result
    }
    vm.calCBM = function (w = 0, l = 0, h = 0) {
        return round(((w * l * h) / 1000000), 1.0)
    }
    vm.addCriteria = function () {
        vm.criteria_list.push({})
    }


    function round(value, step) {
        step || (step = 1.0);
        var inv = 1.0 / step;
        return Math.round(value * inv) / inv;
    }

}


