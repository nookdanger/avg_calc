service_type = [
    {
      "SERVICE_ID": 1,
      "SERVICE_NAME_ENG": "DOOR TO DOOR EXPRESS",
      "SERVICE_NAME_TH":"ส่งเอกสารด่วน"
    },
    {
      "SERVICE_ID": 2,
      "SERVICE_NAME_ENG": "DOOR TO DOOR ECONOMY"
      ,"SERVICE_NAME_TH":"ส่งเอกสาร ประหยัด"
    },
    {
      "SERVICE_ID": 3,
      "SERVICE_NAME_ENG": "DOOR TO DOOR PROMOTION"
      ,"SERVICE_NAME_TH":"ส่งเอกสารด่วน PROMOTION"
    },
    {
      "SERVICE_ID": 4,
      "SERVICE_NAME_ENG": "DOOR TO DOOR EXPRESS"
      ,"SERVICE_NAME_TH":"ส่งพัสดุด่วน"
    },
    {
      "SERVICE_ID": 5,
      "SERVICE_NAME_ENG": "DOOR TO DOOR ECONOMY"
      ,"SERVICE_NAME_TH":"ส่งพัสดุด่วน ประหยัด"
    },
    {
      "SERVICE_ID": 6,
      "SERVICE_NAME_ENG": "DOOR TO DOOR PROMOTION"
      ,"SERVICE_NAME_TH":"ส่งพัสดุด่วน PROMOTION"
    },
    {
      "SERVICE_ID": 7,
      "SERVICE_NAME_ENG": "AIR FREIGHT (AIR CARGO)"
      ,"SERVICE_NAME_TH":"ทางเครื่องบิน"
    },
    {
      "SERVICE_ID": 8,
      "SERVICE_NAME_ENG": "SEA FREIGHT (SEA CARGO LCL)"
      ,"SERVICE_NAME_TH":"ทางเรือไม่เต็มตู้"
    },
    {
      "SERVICE_ID": 9,
      "SERVICE_NAME_ENG": "SEA FREIGHT 20FEET CONT."
      ,"SERVICE_NAME_TH":"ทางเรือเต็มตู้ 20 ฟุต"
    },
    {
      "SERVICE_ID": 10,
      "SERVICE_NAME_ENG": "SEA FREIGHT 40 FEET CONT."
      ,"SERVICE_NAME_TH":"ทางเรือเต็มตู้ 40 ฟุต"
    },
    {
      "SERVICE_ID": 11,
      "SERVICE_NAME_ENG": "SEA FREIGHT 40 FEET HQ CONT."
      ,"SERVICE_NAME_TH":"ทางเรือเต็มตู้สูง 40 ฟุต"
    },
    {
      "SERVICE_ID": 12,
      "SERVICE_NAME_ENG": "IMPORT EXPRESS"
      ,"SERVICE_NAME_TH":"นำเข้าด่วน"
    }
  ]