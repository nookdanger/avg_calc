price_891011 =[
    {
      "COUNTRY": "ALBANIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ANDORRA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "AUSTRALIA",
      "PORT_NAME": "BRISBANE",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 28100,
      "Rate_40_10": 49700,
      "Rate_40HQ_11": 49700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "AUSTRALIA",
      "PORT_NAME": "SYDNEY",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 28100,
      "Rate_40_10": 49700,
      "Rate_40HQ_11": 49700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "AUSTRALIA",
      "PORT_NAME": "MELBOURNE",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 28100,
      "Rate_40_10": 49700,
      "Rate_40HQ_11": 49700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "AUSTRALIA",
      "PORT_NAME": "FREMANTLE",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1500,
      "Rate_20_9": 17300,
      "Rate_40_10": 28100,
      "Rate_40HQ_11": 28100,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "AUSTRIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "BAHRAIN",
      "PORT_NAME": "BAHRAIN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 49700,
      "Rate_40_10": 92900,
      "Rate_40HQ_11": 92900,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "BANGLADESH",
      "PORT_NAME": "CHITTAGON",
      "FIRST_CBM8": 10200,
      "NEXT_CBM8_1": 3900,
      "Rate_20_9": 41600,
      "Rate_40_10": 55100,
      "Rate_40HQ_11": 55100,
      "T_T_Days": 12
    },
    {
      "COUNTRY": "BANGLADESH",
      "PORT_NAME": "DHAKA",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 60500,
      "Rate_40_10": 84800,
      "Rate_40HQ_11": 84800,
      "T_T_Days": 13
    },
    {
      "COUNTRY": "BELGIUM",
      "PORT_NAME": "ANTWERP",
      "FIRST_CBM8": 8200,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 68600,
      "Rate_40_10": 125300,
      "Rate_40HQ_11": 130700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "BHUTAN",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "BRAZIL",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "BRUNEI",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "BULGARIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CAMBODIA",
      "PORT_NAME": "PHNOM PENH",
      "FIRST_CBM8": 9300,
      "NEXT_CBM8_1": 3000,
      "Rate_20_9": 33500,
      "Rate_40_10": 52400,
      "Rate_40HQ_11": 52400,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CAMBODIA",
      "PORT_NAME": "SIHANOUKVILLE",
      "FIRST_CBM8": 8900,
      "NEXT_CBM8_1": 2600,
      "Rate_20_9": 33500,
      "Rate_40_10": 52400,
      "Rate_40HQ_11": 52400,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CANADA",
      "PORT_NAME": "MONTREAL",
      "FIRST_CBM8": 12300,
      "NEXT_CBM8_1": 5800,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CHILE",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "SHANGHAI",
      "FIRST_CBM8": 9600,
      "NEXT_CBM8_1": 900,
      "Rate_20_9": 18650,
      "Rate_40_10": 11900,
      "Rate_40HQ_11": 11900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "XIAMEN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 18650,
      "Rate_40_10": 14600,
      "Rate_40HQ_11": 14600,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "YANTAIN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 18650,
      "Rate_40_10": 11900,
      "Rate_40HQ_11": 11900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "SHANGHAI",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 9200,
      "Rate_40_10": 11900,
      "Rate_40HQ_11": 11900,
      "T_T_Days": 11
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "XINGANG",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 18650,
      "Rate_40_10": 11900,
      "Rate_40HQ_11": 11900,
      "T_T_Days": 14
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "CHONGQING",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 36200,
      "Rate_40_10": 60500,
      "Rate_40HQ_11": 60500,
      "T_T_Days": 14
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "ZHENGZHOU",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 52400,
      "Rate_40_10": 98300,
      "Rate_40HQ_11": 98300,
      "T_T_Days": 14
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "ZHONGSHAN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 25400,
      "Rate_40_10": 44300,
      "Rate_40HQ_11": 44300,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CHINA",
      "PORT_NAME": "HAUANGPU",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 21350,
      "Rate_40_10": 36200,
      "Rate_40HQ_11": 36200,
      "T_T_Days": 14
    },
    {
      "COUNTRY": "COLOMBIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CROATIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CYPRUS",
      "PORT_NAME": "IMASSOL",
      "FIRST_CBM8": 15200,
      "NEXT_CBM8_1": 8600,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "CZECH REP.",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "DENMARK",
      "PORT_NAME": "AARHUS",
      "FIRST_CBM8": 10200,
      "NEXT_CBM8_1": 3900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "DENMARK",
      "PORT_NAME": "COPENHAGEN",
      "FIRST_CBM8": 10200,
      "NEXT_CBM8_1": 3900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "EGYPT",
      "PORT_NAME": "PORT_NAME SAID WEST",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 76700,
      "Rate_40_10": 141500,
      "Rate_40HQ_11": 146900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ESTONIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "FIJI",
      "PORT_NAME": "SUVA,LAUTOKA",
      "FIRST_CBM8": 14500,
      "NEXT_CBM8_1": 7100,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "FINLAND",
      "PORT_NAME": "HELSINKI",
      "FIRST_CBM8": 9300,
      "NEXT_CBM8_1": 3000,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "FRANCE",
      "PORT_NAME": "LE HAVRE",
      "FIRST_CBM8": 8200,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 68600,
      "Rate_40_10": 125300,
      "Rate_40HQ_11": 130700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "FRANCE",
      "PORT_NAME": "FOS SUR MER",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 60500,
      "Rate_40_10": 109100,
      "Rate_40HQ_11": 114500,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "FRANCE",
      "PORT_NAME": "MARSEILLES",
      "FIRST_CBM8": 8600,
      "NEXT_CBM8_1": 2300,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GERMANY",
      "PORT_NAME": "HAMBURG",
      "FIRST_CBM8": 8200,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 68600,
      "Rate_40_10": 125300,
      "Rate_40HQ_11": 130700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GERMANY",
      "PORT_NAME": "Osanbruck",
      "FIRST_CBM8": 10400,
      "NEXT_CBM8_1": 4100,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GERMANY",
      "PORT_NAME": "HANOVER",
      "FIRST_CBM8": 10400,
      "NEXT_CBM8_1": 4100,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GERMANY",
      "PORT_NAME": "HAMBURG",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1500,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GERMANY",
      "PORT_NAME": "OSNABRUECK",
      "FIRST_CBM8": 9300,
      "NEXT_CBM8_1": 3000,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GERMANY",
      "PORT_NAME": "MUNICH",
      "FIRST_CBM8": 9500,
      "NEXT_CBM8_1": 3200,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GHANA",
      "PORT_NAME": "TEMA",
      "FIRST_CBM8": 9800,
      "NEXT_CBM8_1": 3500,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GREECE",
      "PORT_NAME": "PIRAEUS",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 76700,
      "Rate_40_10": 141500,
      "Rate_40HQ_11": 146900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "GREENLAND",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "HONDURAS",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "HONGKONG",
      "PORT_NAME": "HONGKONG",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1500,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "HUNGARY",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ICELAND",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "INDIA",
      "PORT_NAME": "CALCUTTA (KOLKATA)",
      "FIRST_CBM8": 9500,
      "NEXT_CBM8_1": 3200,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "INDIA",
      "PORT_NAME": "MADRAS (CHENNAI)",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "INDIA",
      "PORT_NAME": "NHAVA SHEVA",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "INDIA",
      "PORT_NAME": "NEW DELHI (PPG)",
      "FIRST_CBM8": 9500,
      "NEXT_CBM8_1": 3200,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "INDIA",
      "PORT_NAME": "BOMBAY",
      "FIRST_CBM8": 8900,
      "NEXT_CBM8_1": 2600,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "INDONESIA",
      "PORT_NAME": "JAKARTA",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 900,
      "Rate_20_9": 16220,
      "Rate_40_10": 22700,
      "Rate_40HQ_11": 22700,
      "T_T_Days": 6
    },
    {
      "COUNTRY": "INDONESIA",
      "PORT_NAME": "BELAWAN",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 25400,
      "Rate_40_10": 36200,
      "Rate_40HQ_11": 36200,
      "T_T_Days": 6
    },
    {
      "COUNTRY": "INDONESIA",
      "PORT_NAME": "SEMARANG",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1600,
      "Rate_20_9": 22700,
      "Rate_40_10": 36200,
      "Rate_40HQ_11": 36200,
      "T_T_Days": 7
    },
    {
      "COUNTRY": "INDONESIA",
      "PORT_NAME": "SURABAYA",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1000,
      "Rate_20_9": 20000,
      "Rate_40_10": 36200,
      "Rate_40HQ_11": 36200,
      "T_T_Days": 12
    },
    {
      "COUNTRY": "IRAN",
      "PORT_NAME": "Bandar Abbas",
      "FIRST_CBM8": 10000,
      "NEXT_CBM8_1": 3700,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "IRELAND, REP.OF",
      "PORT_NAME": "DUBLIN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 87500,
      "Rate_40_10": 149600,
      "Rate_40HQ_11": 155000,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ISRAEL",
      "PORT_NAME": "ASHDOD",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 68600,
      "Rate_40_10": 125300,
      "Rate_40HQ_11": 114500,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ISRAEL",
      "PORT_NAME": "HAIFA",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 92900,
      "Rate_40_10": 173900,
      "Rate_40HQ_11": 179300,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ITALY",
      "PORT_NAME": "GENOA",
      "FIRST_CBM8": 8400,
      "NEXT_CBM8_1": 2100,
      "Rate_20_9": 60500,
      "Rate_40_10": 109100,
      "Rate_40HQ_11": 114500,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ITALY",
      "PORT_NAME": "NAPLE",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 79400,
      "Rate_40_10": 146900,
      "Rate_40HQ_11": 152300,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ITALY",
      "PORT_NAME": "LIVORNO",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 79400,
      "Rate_40_10": 146900,
      "Rate_40HQ_11": 152300,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "ITALY",
      "PORT_NAME": "ROMA",
      "FIRST_CBM8": 8700,
      "NEXT_CBM8_1": 2400,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JAMAICA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JAPAN",
      "PORT_NAME": "TOKYO",
      "FIRST_CBM8": 9800,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 17300,
      "Rate_40_10": 20000,
      "Rate_40HQ_11": 20000,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JAPAN",
      "PORT_NAME": "YOKOHAMA",
      "FIRST_CBM8": 9800,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 17300,
      "Rate_40_10": 20000,
      "Rate_40HQ_11": 20000,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JAPAN",
      "PORT_NAME": "KOBE",
      "FIRST_CBM8": 9800,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 17300,
      "Rate_40_10": 20000,
      "Rate_40HQ_11": 20000,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JAPAN",
      "PORT_NAME": "OSAKA",
      "FIRST_CBM8": 9800,
      "NEXT_CBM8_1": 1800,
      "Rate_20_9": 17300,
      "Rate_40_10": 20000,
      "Rate_40HQ_11": 20000,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JAPAN",
      "PORT_NAME": "NAGOYA",
      "FIRST_CBM8": 9800,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 17300,
      "Rate_40_10": 20000,
      "Rate_40HQ_11": 20000,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JAPAN",
      "PORT_NAME": "HIROSHIMA",
      "FIRST_CBM8": 8900,
      "NEXT_CBM8_1": 2600,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "JORDAN",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "KAZAKHSTAN",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "KOREA",
      "PORT_NAME": "BUSAN",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1500,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "KUWAIT",
      "PORT_NAME": "KUWAIT",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 49700,
      "Rate_40_10": 92900,
      "Rate_40HQ_11": 92900,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "KUWAIT",
      "PORT_NAME": "SHUAIBA",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 49700,
      "Rate_40_10": 92900,
      "Rate_40HQ_11": 92900,
      "T_T_Days": 23
    },
    {
      "COUNTRY": "KUWAIT",
      "PORT_NAME": "SHUWAIKH",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 49700,
      "Rate_40_10": 92900,
      "Rate_40HQ_11": 92900,
      "T_T_Days": 23
    },
    {
      "COUNTRY": "LAOS",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "LATVIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "LEBANON",
      "PORT_NAME": "BEIRUT",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 95600,
      "Rate_40_10": 179300,
      "Rate_40HQ_11": 184700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "LIECHTENSTEIN",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "LITHUANIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "LUXEMBOURG",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MACAU",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MALAYSIA",
      "PORT_NAME": "KLANG",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 28100,
      "Rate_40_10": 44300,
      "Rate_40HQ_11": 44300,
      "T_T_Days": 8
    },
    {
      "COUNTRY": "MALAYSIA",
      "PORT_NAME": "PENANG",
      "FIRST_CBM8": 8400,
      "NEXT_CBM8_1": 2100,
      "Rate_20_9": 28100,
      "Rate_40_10": 44300,
      "Rate_40HQ_11": 44300,
      "T_T_Days": 7
    },
    {
      "COUNTRY": "MALAYSIA",
      "PORT_NAME": "PORT_NAME KELANG",
      "FIRST_CBM8": 8200,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MALAYSIA",
      "PORT_NAME": "PARIS GUDANG",
      "FIRST_CBM8": 8600,
      "NEXT_CBM8_1": 2300,
      "Rate_20_9": 28100,
      "Rate_40_10": 38900,
      "Rate_40HQ_11": 38900,
      "T_T_Days": 7
    },
    {
      "COUNTRY": "MALDIVES",
      "PORT_NAME": "MALE",
      "FIRST_CBM8": 10200,
      "NEXT_CBM8_1": 3900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MALTA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MAURITIUS",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MEXICO",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MONACO",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MONGOLIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "MYANMAR",
      "PORT_NAME": "YANGON",
      "FIRST_CBM8": 9800,
      "NEXT_CBM8_1": 3500,
      "Rate_20_9": 55100,
      "Rate_40_10": 103700,
      "Rate_40HQ_11": 103700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "NEPAL",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "NETHERLANDS",
      "PORT_NAME": "ROTTERDAM",
      "FIRST_CBM8": 8200,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 68600,
      "Rate_40_10": 125300,
      "Rate_40HQ_11": 130700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "NEW ZEALAND",
      "PORT_NAME": "AUCKLAND",
      "FIRST_CBM8": 8700,
      "NEXT_CBM8_1": 2400,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "NIGERIA",
      "PORT_NAME": "LAGOS",
      "FIRST_CBM8": 16600,
      "NEXT_CBM8_1": 9900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "NORWAY",
      "PORT_NAME": "OSLO",
      "FIRST_CBM8": 10700,
      "NEXT_CBM8_1": 4400,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "NORWAY",
      "PORT_NAME": "BERGEN",
      "FIRST_CBM8": 11700,
      "NEXT_CBM8_1": 5300,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "OMAN",
      "PORT_NAME": "SOHAR",
      "FIRST_CBM8": 10500,
      "NEXT_CBM8_1": 4200,
      "Rate_20_9": 49700,
      "Rate_40_10": 92900,
      "Rate_40HQ_11": 92900,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "PAKISTAN",
      "PORT_NAME": "KARACHI",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 25400,
      "Rate_40_10": 38900,
      "Rate_40HQ_11": 38900,
      "T_T_Days": 18
    },
    {
      "COUNTRY": "PAPUA NEW GUINEA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "PERU",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "PHILIPPINES",
      "PORT_NAME": "MANILA",
      "FIRST_CBM8": 9600,
      "NEXT_CBM8_1": 3300,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "PHILIPPINES",
      "PORT_NAME": "MANILA (NORTH)",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1000,
      "Rate_20_9": 14600,
      "Rate_40_10": 25400,
      "Rate_40HQ_11": 25400,
      "T_T_Days": 5
    },
    {
      "COUNTRY": "PHILIPPINES",
      "PORT_NAME": "CEBU",
      "FIRST_CBM8": 8200,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 28100,
      "Rate_40_10": 49700,
      "Rate_40HQ_11": 49700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "PHILIPPINES",
      "PORT_NAME": "DAVAO",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 25400,
      "Rate_40_10": 41600,
      "Rate_40HQ_11": 41600,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "PHILIPPINES",
      "PORT_NAME": "SUBIC BAY",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 17300,
      "Rate_40_10": 28100,
      "Rate_40HQ_11": 28100,
      "T_T_Days": 15
    },
    {
      "COUNTRY": "POLAND",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "PORT_NAMEUGAL",
      "PORT_NAME": "LEIXOES",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 76700,
      "Rate_40_10": 130700,
      "Rate_40HQ_11": 136100,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "PORT_NAMEUGAL",
      "PORT_NAME": "LISBON",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 76700,
      "Rate_40_10": 130700,
      "Rate_40HQ_11": 136100,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "QATAR",
      "PORT_NAME": "DOHA",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 65900,
      "Rate_40_10": 117200,
      "Rate_40HQ_11": 117200,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "ROMANIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "RUSSIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SAUDI ARABIA",
      "PORT_NAME": "DAMMAM",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 60500,
      "Rate_40_10": 114500,
      "Rate_40HQ_11": 114500,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "SAUDI ARABIA",
      "PORT_NAME": "RIYADH",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 71300,
      "Rate_40_10": 130700,
      "Rate_40HQ_11": 130700,
      "T_T_Days": 23
    },
    {
      "COUNTRY": "SEYCHELLES",
      "PORT_NAME": "MAHE",
      "FIRST_CBM8": 16300,
      "NEXT_CBM8_1": 9600,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SINGAPORE",
      "PORT_NAME": "SINGAPORE 1",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SINGAPORE",
      "PORT_NAME": "SINGAPORE 2",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SINGAPORE",
      "PORT_NAME": "SINGAPORE",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SLOVENIA",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SOUTH AFRICA",
      "PORT_NAME": "DURBAN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 38900,
      "Rate_40_10": 65900,
      "Rate_40HQ_11": 65900,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "SOUTH AFRICA",
      "PORT_NAME": "CAPE TOWN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 38900,
      "Rate_40_10": 65900,
      "Rate_40HQ_11": 65900,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "SPAIN",
      "PORT_NAME": "BARCELONA",
      "FIRST_CBM8": 8400,
      "NEXT_CBM8_1": 2100,
      "Rate_20_9": 60500,
      "Rate_40_10": 109100,
      "Rate_40HQ_11": 114500,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SPAIN",
      "PORT_NAME": "VALENCIA",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 60500,
      "Rate_40_10": 109100,
      "Rate_40HQ_11": 114500,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SRILANKA",
      "PORT_NAME": "COLOMBO",
      "FIRST_CBM8": 8900,
      "NEXT_CBM8_1": 2600,
      "Rate_20_9": 30800,
      "Rate_40_10": 55100,
      "Rate_40HQ_11": 55100,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "SWEDEN",
      "PORT_NAME": "GOTHENBURG",
      "FIRST_CBM8": 10200,
      "NEXT_CBM8_1": 3900,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SWEDEN",
      "PORT_NAME": "STOCKHOLM",
      "FIRST_CBM8": 10700,
      "NEXT_CBM8_1": 4400,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "SWITZERLAND",
      "PORT_NAME": "",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TAIWAN",
      "PORT_NAME": "KEELUNG",
      "FIRST_CBM8": 8400,
      "NEXT_CBM8_1": 2100,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TURKEY",
      "PORT_NAME": "HAYDARPASA",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 90200,
      "Rate_40_10": 168500,
      "Rate_40HQ_11": 173900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TURKEY",
      "PORT_NAME": "MERSIN",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 90200,
      "Rate_40_10": 168500,
      "Rate_40HQ_11": 173900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TURKEY",
      "PORT_NAME": "EVYAP PORT_NAME",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 90200,
      "Rate_40_10": 168500,
      "Rate_40HQ_11": 173900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TURKEY",
      "PORT_NAME": "ISTALBUL",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 90200,
      "Rate_40_10": 168500,
      "Rate_40HQ_11": 173900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TURKEY",
      "PORT_NAME": "AMBARLI",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 90200,
      "Rate_40_10": 168500,
      "Rate_40HQ_11": 173900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TURKEY",
      "PORT_NAME": "DAMIETTA",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 90200,
      "Rate_40_10": 168500,
      "Rate_40HQ_11": 173900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "TURKEY",
      "PORT_NAME": "PORT_NAME SAID EAST",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 90200,
      "Rate_40_10": 168500,
      "Rate_40HQ_11": 173900,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "UNITED ARAB EMIRATE",
      "PORT_NAME": "JEBEL",
      "FIRST_CBM8": 8400,
      "NEXT_CBM8_1": 2100,
      "Rate_20_9": 28100,
      "Rate_40_10": 49700,
      "Rate_40HQ_11": 49700,
      "T_T_Days": 15
    },
    {
      "COUNTRY": "UNITED ARAB EMIRATE",
      "PORT_NAME": "ABU DHABI",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 49700,
      "Rate_40_10": 92900,
      "Rate_40HQ_11": 92900,
      "T_T_Days": 16
    },
    {
      "COUNTRY": "UNITED ARAB EMIRATE",
      "PORT_NAME": "SHARJAH",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 49700,
      "Rate_40_10": 92900,
      "Rate_40HQ_11": 92900,
      "T_T_Days": 22
    },
    {
      "COUNTRY": "UNITED KINGDOM",
      "PORT_NAME": "SOUTHAMPTON",
      "FIRST_CBM8": 8200,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 68600,
      "Rate_40_10": 125300,
      "Rate_40HQ_11": 130700,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "USA",
      "PORT_NAME": "LAX",
      "FIRST_CBM8": 11300,
      "NEXT_CBM8_1": 3700,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "USA",
      "PORT_NAME": "NYC",
      "FIRST_CBM8": 12000,
      "NEXT_CBM8_1": 4400,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "USA",
      "PORT_NAME": "Houston",
      "FIRST_CBM8": 12000,
      "NEXT_CBM8_1": 5600,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "USA",
      "PORT_NAME": "CHICAGO",
      "FIRST_CBM8": 11400,
      "NEXT_CBM8_1": 5100,
      "Rate_20_9": "",
      "Rate_40_10": "",
      "Rate_40HQ_11": "",
      "T_T_Days": ""
    },
    {
      "COUNTRY": "VIETNAM",
      "PORT_NAME": "HOCHIMINH",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 1900,
      "Rate_20_9": 10280,
      "Rate_40_10": 15680,
      "Rate_40HQ_11": 15680,
      "T_T_Days": 3
    },
    {
      "COUNTRY": "VIETNAM",
      "PORT_NAME": "HAIPHONG",
      "FIRST_CBM8": 8300,
      "NEXT_CBM8_1": 2000,
      "Rate_20_9": 36200,
      "Rate_40_10": 55100,
      "Rate_40HQ_11": 55100,
      "T_T_Days": 10
    },
    {
      "COUNTRY": "VIETNAM",
      "PORT_NAME": "DA NANG",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 33500,
      "Rate_40_10": 60500,
      "Rate_40HQ_11": 60500,
      "T_T_Days": ""
    },
    {
      "COUNTRY": "VIETNAM",
      "PORT_NAME": "PHUOC LONG 1",
      "FIRST_CBM8": "",
      "NEXT_CBM8_1": "",
      "Rate_20_9": 20000,
      "Rate_40_10": 30800,
      "Rate_40HQ_11": 30800,
      "T_T_Days": ""
    }
  ]